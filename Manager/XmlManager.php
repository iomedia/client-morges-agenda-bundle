<?php
/**
 * Fichier permettant la lecture d'un XML et de stocker les informations dans un objets ( utilisé pour les imports de news)
 * @Author Dorien Chan @Iomedia
 */
namespace Morges\Bundle\AgendaBundle\Manager;

use Morges\Bundle\AgendaBundle\Object\CategoryAgenda;
use Morges\Bundle\AgendaBundle\Object\Agenda;

class XmlManager
{
    private $xml;
    private $proxy;
    private $objects; // Contient un objet pour chaque item dans le XML

    public function __construct($proxy)
    {
        $this->xml = new \SimpleXMLElement("http://www.guidle.com/m_381Tpt/eb/Ville-de-Morges/Agend?language=fr", Null, True);
        $this->proxy = $proxy;
    }

    // Création d'un tableau d'objets contenant les informations du XML
    public function init($type)
    {
        $objects = array();

        switch($type){
            case 'category':
                $objects = $this->setCategory($this->xml);
                break;
            case 'agenda':
                $objects = $this->setAgenda($this->xml);
                break;
        }

        $this->setObjects($objects);
    }

    private function setCategory($xml)
    {
        $objects = array();

        $categories = $xml->xpath('/guidle:exportData/guidle:searchCriteria/guidle:what/guidle:kindTag'); //get creation date of the file

        foreach ($categories as $key => $item) {
            $category = new CategoryAgenda();
            $category->setId((int)$item['id']);
            $category->setTitle((string)$item['name']);

            $objects[] = $category;
        }

        return $objects;
    }

    private function setAgenda($xml)
    {
        $objects = array();

        $agendas =  $xml->xpath('/guidle:exportData/guidle:groupSet/guidle:group/guidle:offer');
        $this->proxy->init('fr', 64, 1);
        $myCategAgenda = $this->proxy->getIndex('category_agenda_all',64,null, null, array("sites" => array(0=>1)),array('dateFrom' => 'DESC'),1000,null,1000);

        $this->proxy->init('fr', 45, 1);
        $agendaAll = $this->proxy->getIndex('index_agenda_all',45,null, null, null,null,1000,null,1000);



        $idsToDeleteDate = array();
        $idsToDelete = array();

        // Check si des fiches ont été supprimé de guidle pour pouvoir désactiver les fiches par la suites ( $idsToDelete )
        $idsGuidle = array();
        foreach ($agendas as $key => $offer) {
            $idsGuidle[] = (int)$offer['id'];
        }

        foreach($agendaAll["records"] as $key => $record){
            if(isset($record["id_guidle"]) && !empty($record["id_guidle"]) && strpos($record["id_guidle"], "-") == false){
                if(!in_array($record["id_guidle"], $idsGuidle)){
                    $agenda = new Agenda($myCategAgenda['records']);
                    $agenda->setId($record["id_guidle"]);
                    $agenda->setStatus(0);
                    $objects[] = $agenda;
                }
            }
        }


        foreach ($agendas as $key => $offer) {

            // Check si des dates ont été retiré de l'evenement pour pouvoir désactiver les fiches par la suites ( $idsToDeleteDate )
            $scheldules = array();
            $numberDate = 0;
            foreach ($offer->xpath('./guidle:schedules') as $keysShedules => $shedules) {
                foreach ($shedules->xpath('./guidle:date') as $keyDate => $date) {
                    $numberDate++;
                }
            }

            $multiple = $this->searchOnKeyLike($agendaAll["records"], "id_guidle", (string)$offer['id']);
            foreach ($multiple as $key12 => $value) {
                if ($value["id_guidle"] != (string)$offer['id']) {
                    $multipleId = (int)str_replace((string)$offer['id'] . "-", "", $value["id_guidle"]);
                    if ($multipleId > $numberDate) {
                        $idsToDeleteDate[$value["idContent"]] = $value["id_guidle"];
                    }
                }
            }

            $dateXml = array();
            foreach ($offer->xpath('./guidle:schedules') as $keysShedules => $shedules) {
                foreach ($shedules->xpath('./guidle:date') as $keyDate => $date) {
                    $from = $date->xpath('./guidle:startDate')[0];
                    $to =  $date->xpath('./guidle:endDate')[0];
                    $from = new \DateTime((string)$from);
                    $to = new \DateTime((string)$to);

                    $val = $from->getTimestamp() + $to->getTimestamp();

                    $dateXml[$val] = $date;
                }
            }


            //foreach ($offer->xpath('./guidle:schedules') as $keysShedules => $shedules) {
            $iDate = 1;
            foreach ($dateXml as $keyDate => $date) {
                $agenda = new Agenda($myCategAgenda['records']);
                $agenda->setStatus(2);


                $dateFrom = $date->xpath('./guidle:startDate')[0];
                $dateTo = $date->xpath('./guidle:endDate')[0];

                $timeStart = null;
                if (isset($date->xpath('./guidle:startTime')[0])) {
                    $timeStart = $date->xpath('./guidle:startTime')[0];
                }
                $timeTo = null;
                if (isset($date->xpath('./guidle:endTime')[0])) {
                    $timeTo = $date->xpath('./guidle:endTime')[0];
                }

                $days = null;

                $timeStart = (string)$timeStart;
                if (strlen($timeStart) == 8) {
                    $timeStart = substr($timeStart, 0, 5);
                }

                $timeTo = (string)$timeTo;
                if (strlen($timeTo) == 8) {
                    $timeTo = substr($timeTo, 0, 5);
                }

                $horraire = (string)$timeStart . " - " . $timeTo . $days;

                if (empty($timeTo)) {
                    $horraire = (string)$timeStart . " " . $days;
                }

                if (empty($timeStart) && empty($timeTo) && empty($days)) {
                    $horraire = "";
                }

                $agenda->setHorraire($horraire);
                $agenda->setDateFrom(new \DateTime((string)$dateFrom));
                $agenda->setDateTo(new \DateTime((string)$dateTo));

                //Format les données

                if($iDate == 1) {
                    $agenda->setId((int)$offer['id']);
                }
                else{
                    $idOffer = sprintf( "%s%s", (string)$offer['id'], "-".(string)$iDate);
                    $agenda->setId($idOffer);
                }

                $iDate++;




                // $agenda->setDateFrom(new \DateTime((string)$dateFrom));


                // catégorie
                foreach ($offer->xpath('./guidle:classifications') as $keyCat => $category) {
                    $categAgenda = array();
                    foreach ($category->xpath('./guidle:classification') as $classification) {
                        foreach ($classification->xpath('./guidle:tag') as $keyTag => $tag) {
                            $categAgenda[] = (int)$tag['subcategoryId'];
                        }
                    }
                    $agenda->setCategory($categAgenda);
                }


                // titre, header, description, website, prix, reservations, image
                foreach ($offer->xpath('./guidle:offerDetail') as $keyOffer => $offerDetail) {
                    $title = $offerDetail->xpath('./guidle:title')[0];
                    $header = $offerDetail->xpath('./guidle:shortDescription')[0];
                    $description = $offerDetail->xpath('./guidle:longDescription')[0];
                    $website = $offerDetail->xpath('./guidle:homepage')[0];

                    $priceInfo = null;
                    if (isset($offerDetail->xpath('./guidle:priceInformation')[0])) {
                        $priceInfo = $offerDetail->xpath('./guidle:priceInformation')[0];
                    }

                    $reservations = null;
                    if (isset($offerDetail->xpath('./guidle:ticketingUrl')[0])) {
                        $reservations = $offerDetail->xpath('./guidle:ticketingUrl')[0];
                    }

                    $myImage = null;
                    if (isset($offerDetail->xpath('./guidle:images')[0])) {
                        foreach ($offerDetail->xpath('./guidle:images') as $images) {
                            foreach ($images->xpath('./guidle:image') as $image) {
                                //$myImage = $image->xpath('./guidle:url')[0];
                                foreach ($image->xpath('./guidle:size') as $keySize => $size) {
                                    if($size["label"] == 'medium') {
                                        $splited = explode (".", (string)$size['url']);
                                        if(end($splited) == 'jpg' or end($splited) == 'png') {
                                            $myImage[] = (string)$size['url'];
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $agenda->setTitle(((string)$title));
                    $agenda->setHeader(((string)$header));
                    $agenda->setDescription(((string)$description));
                    $agenda->setWebSite((string)$website);
                    $agenda->setPriceInformations((string)$priceInfo);
                    $agenda->setReservation((string)$reservations);
                    $agenda->setImage($myImage);
                }

                // Horraire
                /* foreach ($offer->xpath('./guidle:schedules') as $keysShedules => $shedules) {
                     if(count($shedules->xpath('./guidle:date')) > 1) {
                         $last = count($shedules->xpath('./guidle:date'))-1;
                         $dateFrom = $shedules->xpath('./guidle:date')[0]->xpath('./guidle:startDate')[0];
                         $dateTo = $shedules->xpath('./guidle:date')[$last]->xpath('./guidle:endDate')[0];

                         $timeStart = null;
                         if (isset($shedules->xpath('./guidle:date')[0]->xpath('./guidle:startTime')[0])) {
                             $timeStart = $shedules->xpath('./guidle:date')[0]->xpath('./guidle:startTime')[0];
                         }
                         $timeTo = null;
                         if (isset($shedules->xpath('./guidle:date')[0]->xpath('./guidle:endTime')[0])) {
                             $timeTo = $shedules->xpath('./guidle:date')[0]->xpath('./guidle:endTime')[0];
                         }
                     }
                     else {
                         $dateFrom = $shedules->xpath('./guidle:date')[0]->xpath('./guidle:startDate')[0];
                         $dateTo = $shedules->xpath('./guidle:date')[0]->xpath('./guidle:endDate')[0];

                         $timeStart = null;
                         if (isset($shedules->xpath('./guidle:date')[0]->xpath('./guidle:startTime')[0])) {
                             $timeStart = $shedules->xpath('./guidle:date')[0]->xpath('./guidle:startTime')[0];
                         }
                         $timeTo = null;
                         if (isset($shedules->xpath('./guidle:date')[0]->xpath('./guidle:endTime')[0])) {
                             $timeTo = $shedules->xpath('./guidle:date')[0]->xpath('./guidle:endTime')[0];
                         }
                     }

                     $days = null;

                     $timeStart = (string)$timeStart;
                     if(strlen($timeStart) == 8){
                         $timeStart = substr($timeStart,0,5);
                     }

                     $timeTo = (string)$timeTo;
                     if(strlen($timeTo) == 8){
                         $timeTo = substr($timeTo,0,5);
                     }

                     $horraire = (string)$timeStart." - ".$timeTo . $days;

                     if(empty($timeTo)) {
                         $horraire = (string)$timeStart. " " . $days;
                     }

                     if(empty($timeStart) && empty($timeTo) && empty($days)){
                         $horraire = "";
                     }

                     $agenda->setHorraire($horraire);

                     $agenda->setDateFrom(new \DateTime((string)$dateFrom));
                     $agenda->setDateTo(new \DateTime((string)$dateTo));
                 }*/

                // Lieux
                $venueName = '';
                foreach ($offer->xpath('./guidle:address ') as $keyAdress => $adress) {
                    $rue = null;
                    if (isset($adress->xpath('./guidle:street')[0])) {
                        $rue = $adress->xpath('./guidle:street')[0];
                    }
                    $localite = $adress->xpath('./guidle:city')[0];
                    $npa = $adress->xpath('./guidle:zip')[0];

                    if (isset($adress->xpath('./guidle:venueName')[0])) {
                        $venueName = (string)$adress->xpath('./guidle:venueName')[0];
                    }


                    $agenda->setRue((string)$rue);
                    $agenda->setLocalite((string)$localite);
                    $agenda->setNpa((string)$npa);
                }

                // adresse d'informations(contact)
                foreach ($offer->xpath('./guidle:contact') as $keyContact => $contact) {
                    $adress = null;
                    if (isset($contact->xpath('./guidle:address_2')[0])) {
                        $adress = $contact->xpath('./guidle:address_2')[0];
                    }
                    if (isset($contact->xpath('./guidle:address_1')[0])) {
                        $adress = $contact->xpath('./guidle:address_1')[0];
                    }

                    $zip = null;
                    if (isset($contact->xpath('./guidle:zip')[0])) {
                        $zip = $contact->xpath('./guidle:zip')[0];
                    }
                    $city = null;
                    if (isset($contact->xpath('./guidle:city')[0])) {
                        $city = $contact->xpath('./guidle:city')[0];
                    }

                    $telephone1 = null;
                    if (isset($contact->xpath('./guidle:telephone_1')[0])) {
                        $telephone1 = $contact->xpath('./guidle:telephone_1')[0];
                    }

                    $telephone2 = null;
                    if (isset($contact->xpath('./guidle:telephone_2')[0])) {
                        $telephone2 = $contact->xpath('./guidle:telephone_2')[0];
                    }

                    $company = null;
                    if (isset($contact->xpath('./guidle:company')[0])) {
                        $company = $contact->xpath('./guidle:company')[0];
                    }

                    $email = null;
                    if (isset($contact->xpath('./guidle:email')[0])) {
                        $email = $contact->xpath('./guidle:email')[0];
                    }

                    $adressInfo = $adress . $zip . " " . $city;

                    $adress = (string)$adress;
                    $company = (string)$company;
                    $zip = (string)$zip;
                    if (!empty($company)) {

                        if (!empty($adress)) {
                            $adress = "" . $adress;
                        }

                        if (!empty($zip)) {
                            $zip = ", " . $zip . " " . $city;
                        }

                        $adressInfo = $company . "," . $adress . $zip;
                    }
                    if (!empty($venueName)) {
                        if (!empty($adress)) {
                            $adress = (string)$adress;
                        }

                        if (!empty($zip)) {
                            $zip = " " . $zip . " " . $city;
                        }

                        $adressInfo = $adress . $zip;
                    }

                    if ($adressInfo == ",  ") {
                        $adressInfo = null;
                    }


                    // var_dump((string)$adressInfo);


                    $agenda->setAddress((string)$adressInfo);
                    $agenda->setTelephone((string)$telephone1);
                    $agenda->setMobile((string)$telephone2);
                    $agenda->setEmail((string)$email);


                }

                // Désactivation des fiches
                if(in_array($agenda->getId(),$idsToDeleteDate)){
                    foreach($idsToDeleteDate as $idToDelete){
                        $newAgenda = clone $agenda;
                        $newAgenda->setId($idToDelete);
                        $newAgenda->setStatus(0);
                        $objects[] = $newAgenda;
                    }
                }

                $objects[] = $agenda;
            }
            //}
        }



        /*var_dump($objects);
        exit;*/



        return $objects;
    }


    private function getDeletedIdsDate()
    {

    }




    private function setObjects($objects)
    {
        $this->objects = $objects;
    }

    /*
     * Retourne les objets créer avec l'XML
     */
    public function getObjects()
    {
        return $this->objects;
    }

    /**
     * Merge plusieurs tableau d'objets
     */
    public function mergeObjects($arrayObjets)
    {
        $arrayMerged = array();
        foreach ($arrayObjets as $keyObj => $objects) {
            foreach ($objects as $key => $object) {
                $arrayMerged[$object->getId()][$object->getLanguage()] = $object;
            }
        }

        return $arrayMerged;
    }

    /**
     * Vide un tableau des items vides
     */
    private function clearArray($array)
    {
        foreach ($array as $key => $item) {
            if($item == ' '){
                unset($array[$key]);
            }
        }
        return $array;
    }


    protected function searchOnKeyLike($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && strpos($array[$key], $value) !== false) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, $this->searchOnKeyLike($subarray, $key, $value));
            }
        }

        return $results;
    }


    protected function searchOnKey($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, $this->searchOnKey($subarray, $key, $value));
            }
        }

        return $results;
    }


}
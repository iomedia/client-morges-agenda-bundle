<?php
/**
 * Fichier permettant l'import de données dans une rubrique
 * @Author Dorien Chan @Iomedia
 */
namespace Morges\Bundle\AgendaBundle\Manager;


use Aio\Bundle\ContentBundle\Entity\Tree;
use Aio\Bundle\ContentBundle\Syncro\SyncroTree;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportManager
{
    /**
     * @var ContainerInterface
     */
	private $container;
	private $object; // Object contenant les données et les informations de la rubrique où importer
	private $synchro;
	private $proxy;
	
	protected function getTree($id)
	{
		$repository = $this->container->getDoctrine()
                 	->getManager()
                 	->getRepository('AioContentBundle:Tree');
		return  $repository->find($id);
	}
	
	public function __construct($container)
	{
		$this->container = $container;
		$this->synchro = $this->container->get( 'aio.content.syncro' );
		$this->proxy = $this->container->get( 'aio.content.proxy' );
	}
	
	// Importer le dans une rubrique
	public function importAio($items, $type)
	{
	    $em = $this->container->getDoctrine()->getManager();
        $repositoryContent = $this->container->getDoctrine()
            ->getManager()
            ->getRepository('AioContentBundle:Content');
        switch($type){
            case 'category':
                $tree = $this->getSynchroTreeCategories();
                break;
            case 'agenda':
                $tree = $this->getSynchroTreeAgenda();
                break;
        }

        $arrayId = array();

        $idGuidles = array();
        // stock les clés du tableau news pour l'utilisé dans la boucle For
        foreach ($items as $key => $value) {
            $arrayId[] = $key;

            if($type == "agenda"){
                if(!empty($value->getId())){
                    $idGuidles[$value->getId()] = $value->getTitle();
                }
            }
        }

        // Lance en enregistrement tout les 50 items
        $numberFor = 0; // nombre de fois à executer l'action
        $numberRecords = 50; // nombre d'item
        $itemNumber = count($items);
        $numberFor = ceil($itemNumber / $numberRecords);

        $iCount = 0;
        $iLimit = $numberRecords;

        for($for=0; $for < $numberFor; $for++) {
            for ($i = $iCount; $i < $iLimit; $i++) {

                if (isset($arrayId[$i])) {

                    $myItem = $items[$arrayId[$i]];

                    switch ($type) {
                        case 'category':
                            $record = $this->setRecordForSynchroCategory($myItem);
                            break;

                        case 'agenda':
                            $this->removeImage($myItem, $repositoryContent, $em);
                            $record = $this->setRecordForSynchroAgenda($myItem);
                            break;
                    }

                    $tree->addContent($record);
                }
            }

            $iLimit = $iLimit + $numberRecords;// On augmente la limite pour les 50 prochains enregistrement
            // import dans AIO
            $tree->saveContents();

            try {
                $this->synchro->importMedias();
            }
            catch (\Exception $e) {
                echo "Une erreur s\'est produite lors de l\'import des medias.";
            }

            if( $idContents = $tree->getIdContents() ) {
                $this->container->get( 'aio.content.tree' )->invalidCache( $tree->getTree(), $idContents ) ;
            }

        }


        if($type == "agenda"){
            $this->desactivateNotInTheFlowContents($tree, $idGuidles);
        }


	}

    // Import de category
    protected function getSynchroTreeCategories()
    {
        $repository = $this->container->getDoctrine()
            ->getManager()
            ->getRepository('AioContentBundle:Tree');

        $tree = $this->getTree(64);

        $tree = $this->synchro->getTree( $tree )
            ->setControls( array(
                /**** Titre ****/
                array(
                    'type' => 'string',
                    'name' => 'title',
                    'label' => 'Titre',
                    'identifier' => true,
                    'searchable' => true,
                    'translated' => false,
                ),
            ))
            ->saveControls();

        return $tree;
    }

    // Import de category
    protected function getSynchroTreeAgenda()
    {
        $repository = $this->container->getDoctrine()
            ->getManager()
            ->getRepository('AioContentBundle:Tree');

        $tree = $this->getTree(45);

        $tree = $this->synchro->getTree( $tree )
            ->setControls( array(
                /**** Catégories Agenda ****/
                array(
                    'type' => 'link',
                    'name' => 'cat_agenda',
                    'label' => 'Catégories Agenda',
                    'render' => 'listbox',
                    'target' => 64,
                ),
                /**** Titre ****/
                array(
                    'type' => 'string',
                    'name' => 'title',
                    'label' => 'Titre',

                    'identifier' => true,
                    'searchable' => true,
                    'translated' => false,
                ),
                /**** En-tête ****/
                array(
                    'type' => 'text',
                    'name' => 'header',
                    'label' => 'En-tête',

                    'wysiwyg' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Description ****/
                array(
                    'type' => 'text',
                    'name' => 'text_1',
                    'label' => 'Description',

                    'wysiwyg' => true,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Rue N° ****/
                array(
                    'type' => 'string',
                    'name' => 'rue',
                    'label' => 'Rue N°',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** NPA ****/
                array(
                    'type' => 'string',
                    'name' => 'npa',
                    'label' => 'NPA',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Localité ****/
                array(
                    'type' => 'string',
                    'name' => 'localite',
                    'label' => 'Localité',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Adresse d'information ****/
                array(
                    'type' => 'text',
                    'name' => 'adresse_information',
                    'label' => 'Adresse d\'information',

                    'wysiwyg' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Horaires ****/
                array(
                    'type' => 'text',
                    'name' => 'horaires',
                    'label' => 'Horaires',

                    'wysiwyg' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Téléphone ****/
                array(
                    'type' => 'string',
                    'name' => 'tel',
                    'label' => 'Téléphone',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Téléphone mobile ****/
                array(
                    'type' => 'string',
                    'name' => 'tel_mobile',
                    'label' => 'Téléphone mobile',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Email ****/
                array(
                    'type' => 'string',
                    'name' => 'email',
                    'label' => 'Email',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Site internet ****/
                array(
                    'type' => 'string',
                    'name' => 'website',
                    'label' => 'Site internet',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Prix informations ****/
                array(
                    'type' => 'text',
                    'name' => 'price_informations',
                    'label' => 'Prix informations',

                    'wysiwyg' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Lien réservation ****/
                array(
                    'type' => 'string',
                    'name' => 'link_reservation',
                    'label' => 'Lien réservation',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Id Guidle ****/
                array(
                    'type' => 'string',
                    'name' => 'id_guidle',
                    'label' => 'Id guidle',

                    'identifier' => false,
                    'searchable' => false,
                    'translated' => false,
                ),
                /**** Guidle ****/
                array(
                    'type' => 'checkbox',
                    'name' => 'guidle',
                    'label' => 'Guidle',
                ),
            ))
            ->saveControls();

        return $tree;
    }

    /**
     * Set les données d'une category pour un import
     */
    protected function setRecordForSynchroCategory($category)
    {
        // récupère l'id
        $idRecord = $category->getId();

        // récupère la date
        $dateFrom = date('Y-m-d H:i');

        $status['fr'] = 2;


        $record = array(
            'key' => $this->synchro->getKey( 'NF_CONTENT:%s',$idRecord),
            'parent' => null,
            'dateFrom' => $dateFrom,
            'dateTo' => null,
            'createdAt' => $dateFrom,
            'updatedAt' => NULL,

            'controls' => array(
                'title' => $category->getTitle(),
                'id_guidle' => $category->getId()
            ),
            'status' => $status

        );

        return $record;
    }

    public function removeImage($agenda, $repository, $em)
    {
        $idRecord = $agenda->getId();
        $key = $this->synchro->getKey( 'NF_CONTENT:%s',$idRecord);
        $idContent = $repository->getIdContentByKey($key);

        if(isset($idContent[$key])) {
            $content = $repository->getContent($idContent[$key]);
            $trashMedia = $this->processMedias($content);
            $this->container->get('aio.content.tree')->saveMedias($content, "image", $trashMedia);
        }
        
    }

    /**
     * Set les données d'un agenda pour un import
     */
    protected function setRecordForSynchroAgenda($agenda)
    {
        // récupère l'id
        $idRecord = $agenda->getId();

        // récupère la date
        $dateFrom = new \DateTime();
        $dateTo = new \DateTime();
        $dateFrom = $dateFrom->format('Y-m-d H:i');
        $dateTo = $dateTo->format('Y-m-d H:i');

        if(is_object($agenda->getDateFrom())) {
            $dateFrom = $agenda->getDateFrom()->format('Y-m-d H:i');
        }
        if(is_object($agenda->getDateTo())) {
            $dateTo = $agenda->getDateTo()->format('Y-m-d H:i');
        }

        $status['fr'] = $agenda->getStatus();


        $record = array(
            'key' => $this->synchro->getKey( 'NF_CONTENT:%s',$idRecord),
            'parent' => null,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'createdAt' => NULL,
            'updatedAt' => NULL,

            'controls' => array(
                //'cat_agenda' => $agenda->getCategory(),
                'title' => $agenda->getTitle(),
                'header' => $agenda->getHeader(),
                'text_1' => $agenda->getDescription(),
                'rue' => $agenda->getRue(),
                'npa' => $agenda->getNpa(),
                'localite' => $agenda->getLocalite(),
                'adresse_information' => $agenda->getAddress(),
                'horaires' => $agenda->getHorraire(),
                'tel' => $agenda->getTelephone(),
                'tel_mobile' => $agenda->getMobile(),
                'email' => $agenda->getEmail(),
                'website' => $agenda->getWebSite(),
                'price_informations' => $agenda->getPriceInformations(),
                'link_reservation' => $agenda->getReservation(),
                'id_guidle' => $agenda->getId(),
                'guidle' => true,

            ),
            'status' => $status,

        );

        $categories = $agenda->getCategory();
        if(!empty($categories)) {
            $record['controls']['cat_agenda'] = $categories;
        }

        $myImage = $agenda->getImage();
        if(!empty($myImage)){
            try {

                $arrImage = array();
                foreach ($myImage as $keyImg => $img){
                    $arrImage[] = $this->synchro->getImage($img);
                }

                $record['medias'] = $arrImage;
            }
            catch (\Exception $e) {
                echo "Une erreur s\'est produite lors d\'une insertion d'image.";
            }
        }

        return $record;
    }


    /**
     * Desactive toute les fiches de contenu pas dans le flux guidle
     *
     * @param SyncroTree $tree
     * @param array $idGuidles
     * @return void
     * @author Francois Cheseaux
     */
    protected function desactivateNotInTheFlowContents(SyncroTree $tree, array $idGuidles){
        $contents = $this->container->get("doctrine.orm.default_entity_manager")
            ->getRepository("AioContentBundle:Content")
            ->createQueryBuilder("content")
            ->select("content.id")
            ->innerJoin("content.properties", "properties")
            ->innerJoin("properties.translations", "values")
            ->andWhere("content.tree = :tree")->setParameter("tree", $tree->getTree())
            ->andWhere("properties.name = :guidle")->setParameter("guidle", "id_guidle")
            ->andWhere("values.stringValue NOT IN (:id_guidles)")->setParameter("id_guidles", array_keys($idGuidles))
            ->getQuery()->getResult();
        ;
        $contents = array_map("current", $contents);
        $this->container->get("aio.content.tree")->attach($tree->getTree(), 1, 0, $contents);
        $this->container->get( 'aio.content.tree' )->invalidCache( $tree->getTree(), $contents ) ;

        echo count($idGuidles)." contenus activés ; ".count($contents)." contenus désactivés";
    }

    protected function processMedias($content)
    {
        $trash = array() ;

        foreach($content->getMedias() as $media) {
            if($idMedia = $media->getId()) {
                $trash[] = $media ;
                $content->removeMedia($media) ;
            }
        }
        return $trash ;
    }
}
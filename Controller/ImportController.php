<?php

namespace Morges\Bundle\AgendaBundle\Controller;

use Gedmo\Translatable\Mapping\Driver\Xml;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Morges\Bundle\AgendaBundle\Manager\ImportManager;
use Morges\Bundle\AgendaBundle\Manager\XmlManager;

class ImportController extends Controller
{
    /**
     * @Route("/import/")
     * @Template()
     */
    public function agendaAction()
    {
    	$import = new ImportManager($this);
        $XmlManager = new XmlManager($this->get('aio.content.proxy'));

        $XmlManager->init("category");
        $categories = $XmlManager->getObjects();
        $import->importAio($categories, 'category');

        $XmlManager->init("agenda");
        $agendas = $XmlManager->getObjects();
        $import->importAio($agendas, 'agenda');

        return array(
        	"action" => "Import terminé."
		);
    }
}

<?php
/**
 * Objets Agenda
 * @Author Dorien Chan @Iomedia
 */
namespace Morges\Bundle\AgendaBundle\Object;

class Agenda
{

    private $id;            // Integer
    private $category;
    private $title;
    private $header;
    private $description;
    private $dateFrom;
    private $dateTo;
    private $rue;
    private $npa;
    private $localite;
    private $address;
    private $horraire;
    private $telephone;
    private $mobile;
    private $fax;
    private $email;
    private $webSite;
    private $price;
    private $prixMin;
    private $priceInformations;
    private $reservation;
    private $image;
    private $status;

    private $categoryAgenda;

    public function __construct($categoryAgenda)
    {
        $this->categoryAgenda = $categoryAgenda;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCategory($category)
    {
        foreach($category as $key => $value){
            $find = false;
            foreach($this->categoryAgenda as $catAgenda)
            {
                if($value == $catAgenda['id_guidle']){
                    $category[$key] = (int)$catAgenda['idContent'];
                    $find = true;
                }
            }
            if($find == false){
                unset($category[$key]);
            }
        }

        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setHeader($header)
    {
        $this->header = $header;
    }

    public function getHeader()
    {
        return $this->header;
    }


    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDateFrom($datefrom)
    {
        $this->dateFrom = $datefrom;
    }

    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    public function getDateTo()
    {
        return $this->dateTo;
    }

    public function setRue($rue)
    {
        $this->rue = $rue;
    }

    public function getRue()
    {
        return $this->rue;
    }

    public function setLocalite($localite)
    {
        $this->localite = $localite;
    }

    public function getLocalite()
    {
        return $this->localite;
    }

    public function setNpa($npa)
    {
        $this->npa = $npa;
    }

    public function getNpa()
    {
        return $this->npa;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        return $this->address;
    }


    public function setHorraire($horraire)
    {
        $this->horraire = $horraire;
    }

    public function getHorraire()
    {
        return $this->horraire;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    public function getFax()
    {
        return $this->fax;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setWebSite($website)
    {
        $this->webSite = $website;
    }

    public function getWebSite()
    {
        return $this->webSite;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrixMin($prixmin)
{
    $this->prixMin = $prixmin;
}

    public function getPrixMin()
    {
        return $this->prixMin;
    }

    public function setPriceInformations($priceInformation)
    {
        $this->priceInformations = $priceInformation;
    }

    public function getPriceInformations()
    {
        return $this->priceInformations;
    }

    public function setReservation($reservation)
    {
        $this->reservation = $reservation;
    }

    public function getReservation()
    {
        return $this->reservation;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }
}

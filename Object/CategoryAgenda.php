<?php
/**
 * Objets Agenda
 * @Author Dorien Chan @Iomedia
 */
namespace Morges\Bundle\AgendaBundle\Object;

class CategoryAgenda
{

    private $id;            // Integer
    private $title;         // String

    public function __construct()
    {

    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this->title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}
